class Pattern {
	    public static void main(String[] args) {
		    
	    	    int no = 1;
	
		    char ch = 'A';

		    for(int i=1 ; i<=4 ; i++){

			    for(int j=1 ; j<=3 ; j++){

				    if(i%2!=0){

					    System.out.print(no + " ");

					    no++;

				    }else{

					    System.out.print(ch + " ");

				    }

				    ch++;

			    }

			    System.out.println();

		    }

	    }

}
