class StringDemo{
	public static void main(String[] args){
		String str1 = "Shashi";
		System.out.println(System.identityHashCode(str1));
		
		String str2 = "Shashi";
		System.out.println(System.identityHashCode(str2));
		
		String str3 = "Shashi";
		System.out.println(System.identityHashCode(str3));
		
		String str4 = "Shashi";
		System.out.println(System.identityHashCode(str4));
	}
}
