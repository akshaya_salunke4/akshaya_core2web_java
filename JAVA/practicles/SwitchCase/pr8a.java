
class OddEven{

	     public static void main(String[] args){

	        int num1 = 4;   
	       	int num2 = 6;
		if (num1 < 0 || num2 < 0) {
	     		System.out.println("Sorry negative numbers not allowed");
	         return;
																                }

               int result = num1 * num2;
	        System.out.println("Result of multiplication: " + result);
 
		   // Using switch case to verify if the result is even or odd
                 switch (result % 2) {
                          case 0:
                           System.out.println("Result is even");
			   break;

			  default:
			   System.out.println("Result is odd");
										
		 }
											
	     }
											
}
