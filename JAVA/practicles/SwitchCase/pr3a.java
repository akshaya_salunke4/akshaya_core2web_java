
class Clothes{
	public static void main(String[] args){

		char size = 'S';

		switch(size){
			case 'S':
				System.out.println("Small");
				break;
			
			case 'M':
				System.out.println("Medium");
				break;
			case 'L':
				System.out.println("Large");
				break;
		
			default:
				System.out.println("Unknown Size");
		}
	}
}
