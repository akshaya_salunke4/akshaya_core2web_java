
class Remarks{
	public static void main(String[] args){

		char grade = 'A';

		if(grade == 'O'){
			System.out.println("Outstanding!");
		}
		else if(grade == 'A'){
			System.out.println("Excellent!");
		}
		else if(grade == 'B'){
			System.out.println("Very Good!");
		}
		else if(grade == 'C'){
			System.out.println("Good!");
		}
		else if(grade == 'D'){
			System.out.println("Needs Improvement!");
		}else{
			System.out.println("Invalid Grade!");
		}
	}
}

