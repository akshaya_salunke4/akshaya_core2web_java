
class NaturalNumbers{
	public static void main(String[] args){
		int number = 1;
		int count = 0;
		
		System.out.println("Cube of the first 10 natural numbers:");
		while(count <= 10){
			 int cube = number * number * number;
			 System.out.println("cube of "+ number + " = " + cube );
			 number++;
			 count++;
		}
	}
}
