
class Divisible{
	public static void main(String[] args){
		int start = 100;
		int end = 24;
		System.out.println("Numbers in the range 100 to 24 divisible by both 4 and 5:");

		     while(start >= end){
			     if(start % 4 == 0 && start % 5 == 0){
				     System.out.println(start);
			     }
			     start--;
		     }
	}
}
