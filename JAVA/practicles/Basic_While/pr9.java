
class SumofOddNumbers{
	public static void main(String[] args){
		int start = 150;
		int end = 101;
		int sum = 0;

		System.out.println("Sum of odd numbers from 150 to 101:");
		  while(start >= end){
			  if(start % 2 != 0){
				  sum += start;
			  }
			  start--;
		  }
		  System.out.println("Sum: " + sum);
	}
}
