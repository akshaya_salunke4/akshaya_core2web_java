import java.util.*;
class Reverse{
	     public static void main(String[] args){
	    Scanner sc = new Scanner(System.in);
            System.out.print("Enter a Size:");
            int size = sc.nextInt();
            int[]arr = new int[size];

	    System.out.println("Enter elements:");
	    int temp;
	    for(int i = 0; i < size/2; i++){
		    int temp = arr[i];
		    arr[i] = arr[size-1-i];
		    arr[size-1-i] = temp;
	    }
	    for(int i = 0; i < size/2; i++){
		    System.out.println("Reverse array:" + arr);
	    }
	     }
}
