

class Number{
	public static void main(String[] args){
		int num1 = 10;
		int num2 = 15;

		if (num1 % 2 == 0 && num1 % 5 == 0 && num1 % 10 == 0){
			System.out.println(num1 + " is divisible by 2,5, and 10.");
		}else{
			System.out.println(num1 + " is not divisible by 2,5, and 10.");
		}
		if (num2 % 2 == 0 && num2 % 5 == 0 && num2 % 10 == 0){
			System.out.println(num2 + " is divisible by 2,5, and 10.");
		}else{
			System.out.println(num2 + " is not divisible by 2,5, and 10.");
		}
	}
}


