import java.util.*;
class Pattern6{
	       public static void main(String[] args){
               Scanner sc = new Scanner(System.in);
	       System.out.println("Enter No of Rows: ");
	       int row = sc.nextInt();
	       int no = 1;
	       char ch = 'a';

               for(int i = 1; i <= row;i++){
		       for(int j = row; j >= i; j--){
				if(j%2==0){
					System.out.print(ch + " ");
					ch++;
				}else{
					System.out.print(no + " ");
					no++;
				}	
		       }
		       no = 1;
		       ch = 'a';
		       	
			       System.out.println();
	       }
	       }
}


