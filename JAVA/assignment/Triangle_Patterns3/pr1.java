
import java.util.*;
class Pattern1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No of Rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
		   for(int j = i; j <= row; j++){
			   System.out.print(j + " ");
		   }
			   System.out.println();
		}
	}
}

