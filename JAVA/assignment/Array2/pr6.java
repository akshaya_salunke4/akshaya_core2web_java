import java.util.Scanner;
class Product{
	        public static void main(String[] args){

			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the size :");

			int size = sc.nextInt();

			int[] array = new int[size];

			System.out.println("Enter Elements :");

			for(int i = 0;i < size ; i++){

				array[i] = sc.nextInt();

			}

			int prod = 1;

			for(int i = 1;i < size; i++){
				if(i % 2 == 1){

				prod = prod * array[i];

			}
		}

			System.out.println("Product of odd indexed elements :" + prod);

		}

}
