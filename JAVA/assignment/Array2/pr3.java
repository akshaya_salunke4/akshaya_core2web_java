import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		System.out.print("Enter array elements: ");
		char ch1[] = new char[size];
		for(int i = 0; i < size; i++){
			ch1[i]=(char)br.read();
		}
		for(int i = 0; i < size; i++){
			if(ch1[i]=='a' || ch1[i]=='i' || ch1[i]=='E' || ch1[i]=='O' || ch1[i]=='u'){
				System.out.println("Vowel " + ch1[i]  +  " is found at index " + i);
			}
		}
	}
}

