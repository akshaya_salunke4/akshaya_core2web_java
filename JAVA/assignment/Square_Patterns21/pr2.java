import java.util.*;
class Pattern2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a row:");
		int row = sc.nextInt();
		int temp = row;

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(temp % 3 == 0){
					System.out.print(temp * 3 + " ");
				}else if(temp % 5 == 0){
					System.out.print(temp * 5 + " ");
				}else{
					System.out.print(temp + " ");
				}
					temp++;
				}
				System.out.println();
			}
		}
	}



