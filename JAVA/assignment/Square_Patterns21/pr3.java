import java.util.*;
class Pattern3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a row:");
		int row = sc.nextInt();
		int temp = row;

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(j == 1){
					System.out.print(temp * temp + " ");
				}else{
					System.out.print(temp + " ");
				}
				++temp;
			}
			System.out.println();
		}
	}
}
