import java.io.*;
class Array7{
	      public static void main(String[] args)throws IOException{
	      BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	      System.out.print("Enter size : ");

	      int size=Integer.parseInt(br.readLine());

	      System.out.print("Enter elements : ");

	      char arr[]=new char[size];

	      for(int i=0;i<size;i++){

		      arr[i]=(char)br.read();

	      }

	      for(int i=0;i<size;i++){

		      if(arr[i]>='a' && arr[i]<='z'){

			      arr[i]=(char)(arr[i]-32);

		      }

	      }

	      System.out.println("Concerted array:");

	      for(char c:arr){

		      System.out.print(c+" ");

	      }

	      }

}
