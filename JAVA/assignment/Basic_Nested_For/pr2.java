
class Pattern2 {
	    public static void main(String[] args) {
		          
		    int numberOfRows = 4; 
						  
		for (int row = 1; row <= numberOfRows; row++) {
		 for (int column = 1; column <= numberOfRows; column++) {
			 System.out.print(column + " ");
	         }
		    System.out.println();
	  
	 	}
						  
	    }
}
