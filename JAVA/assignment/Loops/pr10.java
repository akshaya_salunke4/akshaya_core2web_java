
class For{
	public static void main(String[] args){

		System.out.println("Using for loop:");
		        for (int i = 1; i <= 5; i++) {
				            System.out.println(i);
					            }

	         System.out.println("Using while loop:");
	                int j = 1;
                            while (j <= 5) {
			      System.out.println(j);
				     j++;
		       	 }
	}
}
