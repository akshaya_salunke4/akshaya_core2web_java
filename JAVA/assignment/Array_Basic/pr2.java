import java.io.*;
class Array2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[] = new int[10];
		System.out.print("Enter elements:");
		for(int i = 0; i < arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Array elements:");
		for(int i = 0; i < arr.length; i++){
		    System.out.print(arr[i]+ " , ");
		}
	}
}
