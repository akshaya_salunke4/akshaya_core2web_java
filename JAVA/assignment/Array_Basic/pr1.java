:wq:wqss ArrayDemo1{
	public static void main(String[] args){
		int[] array = {10,20,30,40,50,60,70,80,90,100};

		System.out.print("Array:\n");
		for(int i = 0; i < array.length;i++){
			System.out.print(array[i]);
			if(i < array.length - 1){
				System.out.print(",");
			}
		}
	}
}
