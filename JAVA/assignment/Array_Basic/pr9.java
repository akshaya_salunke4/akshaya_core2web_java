import java.util.Scanner;
class OddIndexed {
	    public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
	   System.out.print("Enter size: ");
          int size = sc.nextInt();
          int[] array = new int[size];

	     System.out.println("Enter " + size + " elements:");
	     for (int i = 0; i < size; i++) {
		     System.out.print("Enter element " + (i + 1) + ": ");

		     array[i] = sc.nextInt();

	     }

	     System.out.println("Output:");


	     for (int i = 1; i < size; i += 2) {

		     System.out.println(array[i] + " is an odd indexed element");

	     }


	
	    }

}
