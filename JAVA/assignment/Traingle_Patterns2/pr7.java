
import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No of Rows: ");
		int row = sc.nextInt();
		
			for(int i = 1; i <= row; i++){
		         	int num = 1;
				char ch = 'B';
			    for(int j = 1; j <= i; j++){
				    if(i % 2 == 1){
					    System.out.print(num + " ");
				    }else{
					    System.out.print(ch + " ");
				    }
				    num++;
				    ch++;
			    }
			 System.out.println();
			}
	}
}

