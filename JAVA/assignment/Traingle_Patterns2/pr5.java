
import java.util.*;
class Pattern5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No of Rows: ");
		int row = sc.nextInt();
			for(int i = 1; i <= row; i++){
				char ch1 = 'c';
				char ch2 = 'C';
			   for(int j = 1; j <= i; j++){
				   if(i % 2 == 1){
					   System.out.print(ch1 + " ");
				   }else{
					   System.out.print(ch2 + " ");
				   }
				   ch1--;
				   ch2--;
			   }
			   System.out.println();
			}
	}
}


