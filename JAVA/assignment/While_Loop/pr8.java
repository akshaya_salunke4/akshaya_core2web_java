
class OddDigit {
	    public static void main(String[] args) {
		         
		    int number = 216985;

	              System.out.print("Output: ");
		         while (number > 0) {
			    int digit = number % 10;   
                              if (digit % 2 != 0) {
				      System.out.print(digit + " ");
			      }
		             number /= 10; 
			 }

	    }

}
