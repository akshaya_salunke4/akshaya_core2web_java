import java.util.*;
class Pattern8{
	      public static void main(String[] args){
	      Scanner sc = new Scanner(System.in);
	      System.out.println("Enter no of rows:");
	      int row = sc.nextInt();

	      char ch = 'F';

	       for(int i = 0; i < row; i++){
		       for(int j = 0; j < row-i; j++){
			       System.out.print(ch + " ");
			       ch--;
		       }
		       System.out.println();
	       }
	      }
}

