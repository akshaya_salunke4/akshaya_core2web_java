import java.util.*;
class Pattern6{
	      public static void main(String[] args){
	       Scanner sc = new Scanner(System.in);
	       System.out.println("Enter no of rows:");
	       int row = sc.nextInt();

	        for(int i = 1; i <= row; i++){
		       char ch = 'c';
	               int num = 3;
	         for(int j = 1; j <= i; j++){
	               if(i % 2==1){
	                  System.out.print(ch + " ");
		       }else{
                          System.out.print(num + " ");
		       }
                            ch--;
                            num--;
		 }
                       System.out.println();	
		}
	      }
}	      

