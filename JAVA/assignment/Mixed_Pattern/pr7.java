import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of rows: ");
		int row = sc.nextInt();

		int number = 2;

		for(int i = 0; i < row; i++){
			for(int j = 0; j < row-i; j++){
				System.out.print(number + " ");
				number += 2;
			}
			System.out.println();
		}
	}
}

