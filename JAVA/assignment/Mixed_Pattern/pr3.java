import java.util.*;
class Pattern3{
	      public static void main(String[] args){
	       Scanner sc = new Scanner(System.in);
	       System.out.println("Enter no of rows:");
	       int row = sc.nextInt();

	       for(int i = 1; i <= row; i++){
		       int num = 1;
		       char ch = 'C';
		       for(int j = 1; j <= row; j++){
			       if(i % 2 == 1){
				       System.out.print(ch-- + " ");
			       }else{
				       System.out.print(num++ + " ");
			       }
		       }
		       System.out.println();
	       }
	      }
}
