import java.util.*;
class Pattern2{
	      public static void main(String[] args){
	       Scanner sc = new Scanner(System.in);
	       System.out.println("Enter no of rows:");
	       int row = sc.nextInt();

	       char ch = 'C';
	       int num = 3;
	       for(int i = 1; i <= row; i++){
		       for(int j = 1; j <= row; j++){
			       System.out.print("C" + num-- + " ");
		       }
		       num += 4;
		       System.out.println();
	       }
	      }
}

