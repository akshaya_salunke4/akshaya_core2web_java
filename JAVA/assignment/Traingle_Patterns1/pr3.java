import java.util.*;
class Pattern3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No of Rows: ");
		int row = sc.nextInt();

		char startChar = 'A';
			for(int i= 1; i <= row; i++){
				char printChar = startChar;
			  for(int j= 1; j <= i; j++){
				  System.out.print(printChar + " ");
				  printChar++;
			  }
				  System.out.println();
				  startChar++;
			}
	}
}


