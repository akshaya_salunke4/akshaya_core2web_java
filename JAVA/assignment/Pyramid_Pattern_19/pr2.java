import java.util.*;
class Pattern2 {
           public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
	   System.out.print("Rows = ");
	   int rows = sc.nextInt();


	   int num = 1;
	   for(int i=1; i<=rows; i++){
		   for(int j=rows; j>=i; j--){			
			   System.out.print("  ");
		   }
		   for(int j=1; j<=i+i-1; j++){
			   System.out.print(num++ + " ");
		   }
		   System.out.println();
	   }
	   }

}
