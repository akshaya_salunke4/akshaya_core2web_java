import java.util.*;
class Pattern10 {
	        public static void main(String[] args){
	  	Scanner sc = new Scanner(System.in);

	
		System.out.print("Rows = ");

		int rows = sc.nextInt();


		
		for(int i=rows; i>=1; i--){


			
			int ch = 64 + i;

			for(int j=1; j<=i-1; j++)

				System.out.print("  ");

			for(int j=rows; j>=i; j--)

				System.out.print((char)ch++ + " ");

			ch--;

			for(int j=i; j<=rows-1; j++)

				System.out.print((char) --ch + " ");

			System.out.println();

		}

		}

}
