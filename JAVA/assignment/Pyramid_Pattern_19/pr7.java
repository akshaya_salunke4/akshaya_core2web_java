import java.util.*;
class Pattern7 {
           public static void main(String[] args) {
     	   Scanner sc = new Scanner(System.in);
	   System.out.print("Rows = ");
	   int rows = sc.nextInt();


	   for(int i=1; i<=rows; i++){
		   int ch = 64+i;
		   for(int j=rows-1; j>=i; j--)
			   System.out.print("  ");
		   for(int j=1; j<=i; j++){
			   if(i%2 == 1)
				   System.out.print(i + " ");
			   else
				   System.out.print((char)ch + " ");
		   }
		   for(int j=1; j<=i-1; j++){
			   if(i%2 == 1)
				   System.out.print(i + " ");
			   else
				   System.out.print((char)ch + " ");
		   }
		   System.out.println();
	   }
	   }

}

