import java.util.*;
class Pattern1 {
    	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	System.out.print("Rows = ");
	int rows = sc.nextInt();

	for(int i=1; i<=rows; i++){
		for(int j=rows; j>=1; j--){
			if(j>i)
				System.out.print(" ");
			else
				System.out.print("1");
		}
		for(int j=2; j<=i; j++){
			System.out.print("1");
		}
		System.out.println();
	}
	}

}
