
class ProductofOddDigits{
	 public static void main(String[] args) {
		        
		 int number = 256985;
		 int originalNumber = number;
		 int product = 1; 
							      
		 while (number > 0) {
	          int digit = number % 10; 
		  if (digit % 2 != 0) { 
		  product *= digit; 
		 }
		 number /= 10; 
	  }
		 System.out.println("Product of odd digits: " + product);
							      
	 }
							      
}
