
class Cubes{
	 public static void main(String[] args) {
		        
		 int number = 216985;
	         int originalNumber = number; 
					
	       	 System.out.print("Cubes of even digits: ");
							      
		  while (number > 0) {
	          int digit = number % 10; 
		if (digit % 2 == 0) { 
			int cube = digit * digit * digit;
                    System.out.print(cube + " ");
	     }
		 number /= 10; 
	 }
		 System.out.println(); 
							      
	 }
							      
}
