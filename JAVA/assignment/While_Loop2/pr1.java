
class Divisible {
	    public static void main(String[] args) {
		         
		    int number = 2569185;
	            int originalNumber = number; 
	         
		    System.out.print("Digits divisible by 2 are: ");
								 
                       while (number > 0) {
		          int digit = number % 10; 
		            if (digit % 2 == 0) { 
			 System.out.print(digit + " ");
		      }
			  number /= 10; 
		  }
								 
		   System.out.println(); // Print a newline after the loop
	  }
     }
