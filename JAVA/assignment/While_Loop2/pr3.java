
class Demo{
	public static void main(String[] args) {
		     
	     	int number = 43678051;
		int originalNumber = number; 
                
	        	System.out.print("Digits divisible by 2 or 3 are: ");
							     
			while (number > 0) {
                          int digit = number % 10;  
			  if (digit % 2 == 0 || digit % 3 == 0) { 
			 System.out.print(digit + " ");
							                                                                          
			  }
			  number /= 10; 

			}

                        System.out.println();  
                  }

}
