
class Calculate{
	public static void main(String[] args) {
		       
	       	int number = 2469185;
	        int sumOfSquares = 0;

		 while (number > 0) {
		 int digit = number % 10; 
		 if (digit % 2 != 0) { 
		  sumOfSquares += digit * digit; 
                    }
		 number /= 10; 
	 }
										     
	   System.out.println("Sum of squares of odd digits: " + sumOfSquares);
										     
	}
										     
}
