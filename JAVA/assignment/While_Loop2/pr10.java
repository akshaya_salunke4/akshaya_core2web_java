
class SumOddDigitProduct{
	 public static void main(String[] args) {
		        
		 int number = 9367924;
	         int sumOfOddDigits = 0;
	         int productOfEvenDigits = 1;

		while (number > 0) {
		      	int digit = number % 10; 
											      
		 if (digit % 2 == 0) { 
                  productOfEvenDigits *= digit; 
	          } else { 
		  sumOfOddDigits += digit; 
	    }
											      
		  number /= 10; 
	  }
		System.out.println("Sum of odd digits: " + sumOfOddDigits);
	        System.out.println("Product of even digits: " + productOfEvenDigits);
											      
	 }
											      
}
