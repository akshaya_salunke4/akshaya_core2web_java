
class NotDivisible{
	public static void main(String[] args) {
		      
	      	int number = 2569185;
	        int originalNumber = number;
		
	     	System.out.print("Digits not divisible by 3 are: ");
							     
			 while (number > 0) {
		         int digit = number % 10; 
			  if (digit % 3 != 0) { 
			    System.out.print(digit + " ");
			  }
			    number /= 10; 
		 }
							     
			    System.out.println(); 
							                                            
	}
							     
}
