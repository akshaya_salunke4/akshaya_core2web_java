
class Square{
	public static void main(String[] args) {
		       
	       	int number = 256985;
	        int originalNumber = number; 
	
	      	System.out.print("Square of odd digits: ");
							     
		  while (number > 0) {
	          int digit = number % 10; 
		   if (digit % 2 != 0) { 
		 int square = digit * digit;
		     System.out.print(square + " ");
		   }
							     
		   number /= 10; 
							     
		  }

		  System.out.println(); 
							     
	}
							     
}
