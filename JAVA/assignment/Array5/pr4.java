import java.util.Scanner;
class Array4{
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size of the array:"); 
	    int size = scanner.nextInt();
	    int[] arr = new int[size];
            System.out.println("Enter the elements of the array:");
            for (int i = 0; i < size; i++) {
	    arr[i] = scanner.nextInt();
	    }
             	    int duplicateIndex = -1;
		    for (int i = 0; i < size - 1; i++) {
                      for (int j = i + 1; j < size; j++) {
                          if (arr[i] == arr[j]) {
                               duplicateIndex = i;
			       break;
                 	    }

		   }
                       if (duplicateIndex != -1) {
                 	   break;
                  }
	}
	if (duplicateIndex != -1) {
		System.out.println("First duplicate element present at index " + duplicateIndex);
         } else {
         	System.out.println("No duplicate elements found in the array: ");
	}	

}
}
	  
