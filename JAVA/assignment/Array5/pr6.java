import java.util.Scanner;
class Array6{
	    public static void main(String[] args) {
           Scanner scanner = new Scanner(System.in);
           System.out.println("Enter the size of the array:");
	  int size = scanner.nextInt();
          int[] arr = new int[size];
	  System.out.println("Enter the elements of the array:");
	   for (int i = 0; i < size; i++) {
		   arr[i] = scanner.nextInt();
	   }

	   int primeIndex = -1;
	   for (int i = 0; i < size; i++) {
		   if (arr[i] <= 1) {
			   continue; 
		   }
		   boolean isPrime = true;
		   for (int j = 2; j <= Math.sqrt(arr[i]); j++) {
			   if (arr[i] % j == 0) {
				   isPrime = false;
				   break;
			   }
		   }
		   if (isPrime) {
			   primeIndex = i;
			   break;
		   }
	   }
			   if (primeIndex != -1) {
				  System.out.println("First prime number found at index " + primeIndex);
			   } else {
                                  System.out.println("No prime number found in the array.");
			   }
	    }
}
		  



