import java.util.Scanner;
class Array7{
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size:");
	   int size = scanner.nextInt();
	   int[] arr = new int[size];
	    System.out.println("Enter the elements of the array:");
	    for (int i = 0; i < size; i++) {
          	   arr[i] = scanner.nextInt();
              }
             System.out.print("Composite numbers in the array are: ");
	          boolean found = false;
      		  for (int i = 0; i < size; i++) {
			  int num = arr[i];
			  if (num <= 1) {
				  continue;
			  }
			  boolean isComposite = false;
			  for (int j = 2; j <= Math.sqrt(num); j++) {
				  if (num % j == 0) {
					  isComposite = true;
					  break;
				  }
			  }
			  if (isComposite) {
				  found = true;
				  System.out.print(num + " ");
			  }
		  }
		  if (!found){
                        System.out.println("No composite numbers found in the array.");
		  }
	    }
}
				              










