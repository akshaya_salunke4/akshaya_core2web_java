 import java.util.Scanner;
 class Array10{
	     public static void main(String[] args) {
             Scanner scanner = new Scanner(System.in);
	     System.out.println("Enter the size of the array:");
	      int size = scanner.nextInt();
	       int[] arr = new int[size];
       	       System.out.println("Enter the elements of the array:");
	       for (int i = 0; i < size; i++) {
		       arr[i] = scanner.nextInt();
	       }
	       System.out.print("Output: ");
       	       for (int i = 0; i < size; i++) {
		       int factorial = 1;
		       for (int j = 1; j <= arr[i]; j++) {
			       factorial *= j;
		       }
		       System.out.print(factorial);
		       if (i < size - 1) {
			       System.out.print(", ");
		       }
	       }
	     }
 }
