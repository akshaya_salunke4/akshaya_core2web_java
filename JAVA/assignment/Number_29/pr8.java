import java.util.Scanner;
class DuckNumber {
	    public static void main(String[] args) {
		    Scanner scanner = new Scanner(System.in);
		    System.out.print("Enter a number: ");
		    String number = scanner.next();
		    boolean isDuck = false;
		     if (number.contains("0")) {
			    
			    if (number.charAt(0) != '0') {
				    isDuck = true;
			    }
		    }
		    if (isDuck) {
			    System.out.println(number + " is a Duck Number.");
		    } else {
			    System.out.println(number + " is not a Duck Number.");
		    }
	    }
}
