import java.util.Scanner;
class DeficientNumber {
	    public static void main(String[] args) {
		    Scanner scanner = new Scanner(System.in);
		    System.out.print("Enter a number: ");
		    int number = scanner.nextInt();
		    int sum = 0;
		    for (int i = 1; i < number; i++) {
			    if (number % i == 0) {
			    sum += i;
			    }
		    }
		    if (sum < number) {
			    System.out.println(number + " is not a Deficient Number.");
		    } else {
			    System.out.println(number + " is a Deficient Number.");
		    }
	    }
}
