import java.util.Scanner;
class AutomorphicNumber {
	    public static void main(String[] args) {
		    Scanner scanner = new Scanner(System.in);
		    System.out.print("Enter a number: ");
		    int number = scanner.nextInt();
                    int square = number * number;
                    String numStr = String.valueOf(number);
                    String squareStr = String.valueOf(square);
                    if (squareStr.endsWith(numStr)) {
			    System.out.println(number + " is an Automorphic Number.");
                        } else {
                 	    System.out.println(number + " is not an Automorphic Number.");
                   }
            }
}
