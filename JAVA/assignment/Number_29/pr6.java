import java.util.Scanner;
class FibonacciSeries {
	    public static void main(String[] args) {
	    Scanner scanner = new Scanner(System.in);
	    System.out.print("Enter a number: ");
	    int n = scanner.nextInt();
	    System.out.print("Fibonacci Series: ");
	    int a = 0;
	    int b = 1;
	    for (int i = 0; i < n; i++) {
		    System.out.print(a + ", ");
			    int temp = a + b;
			    a = b;
			    b = temp;
		    }
	    }
}
