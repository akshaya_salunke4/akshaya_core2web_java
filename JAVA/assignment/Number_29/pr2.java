import java.util.*;
class StrongNumber {
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter a number: ");
	    int number = scanner.nextInt();
	    int originalNumber = number;
	    int sum = 0;
	    for (int temp = number; temp > 0; temp /= 10) {
		    int digit = temp % 10;
		    int factorial = 1;
		       for (int i = 1; i <= digit; i++) {
		 	       factorial *= i;
		       }
                 	       sum += factorial;
	              }
       	    if (sum == originalNumber) {
			    System.out.println(originalNumber + " is a Strong Number.");
               } else {
                 	    System.out.println(originalNumber + " is not a Strong Number.");
	       }

	    }

}



