import java.util.Scanner;
class HappyNumber {
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
    	    System.out.print("Enter a number: ");
	    int number = scanner.nextInt();
	    int originalNumber = number;
	    boolean seen = false;
	    for (; number != 1 && !seen;) {
		    int sum = 0;
		    for (int temp = number; temp > 0; temp /= 10) {
			    int digit = temp % 10;
			    sum += digit * digit;
		    }
		    if (sum == originalNumber) {
			    seen = true;

		    }
		    number = sum;
	    }

	    if (number == 1) {
		    System.out.println(originalNumber + " is a Happy Number.");
	    } else {
		    System.out.println(originalNumber + " is not a Happy Number.");
	    }
	    }
}
