import java.util.Scanner;
class ArmstrongNumber {
	    public static void main(String[] args) {
		    Scanner scanner = new Scanner(System.in);
		    System.out.print("Enter a number: ");
		    int number = scanner.nextInt();
	            int originalNumber = number;
		    int numberOfDigits = 0;
		    int temp = number;
		     while (temp != 0) {
			     numberOfDigits++;
			     temp /= 10;
		     }
		     int sum = 0;
		     temp = number;
		      while (temp != 0) {
		      int digit = temp % 10;
		      int power = 1;
		      int count = numberOfDigits;
		      while (count > 0) {
			      power *= digit;
			      count--;
		      }
		      sum += power;
			      temp /= 10;
		      }
		       if (sum == originalNumber) {
			       System.out.println(originalNumber + " is an Armstrong Number.");
		       } else {
			       System.out.println(originalNumber + " is not an Armstrong Number.");
		       }
	    }
}


