import java.util.Scanner;
class Array4{
       public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter the size of the arrays:");
      int size = sc.nextInt();
      System.out.println("Enter the elements of the first array:");
      int[] arr1 = new int[size];
      for (int i = 0; i < size; i++) {
	      arr1[i] = sc.nextInt();
      }
      System.out.println("Enter the elements of the second array:");
      int[] arr2 = new int[size];
      for (int i = 0; i < size; i++) {
	      arr2[i] = sc.nextInt();
      }

      System.out.print("Common elements in the given arrays are: ");
      for (int i = 0; i < size; i++) {
	      for (int j = 0; j < size; j++) {
		      if (arr1[i] == arr2[j]) {
                          System.out.println(arr1[i] + ", ");
			      break;
		      }
	      }
      }

  }
}
