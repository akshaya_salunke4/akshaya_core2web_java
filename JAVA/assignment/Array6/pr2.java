import java.util.Scanner;
class Array2{
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
	    System.out.println("Enter the size of the array:");
	    int size = scanner.nextInt();
	    int[] arr = new int[size];
            System.out.println("Enter the elements of the array:");
            for (int i = 0; i < size; i++) {
         	    arr[i] = scanner.nextInt();
             }
	    int sumOfPrimes = 0;
            int primeCount = 0;
           for (int i = 0; i < size; i++) {
  	    if (arr[i] > 1) {
	         boolean isPrime = true;
		    for (int j = 2; j <= Math.sqrt(arr[i]); j++) {
				    if (arr[i] % j == 0) {
					    isPrime = false;
					    break;
				    }
		    }
                	    if (isPrime) {
              		    sumOfPrimes += arr[i];
			    primeCount++;
			    }
		    }
	    }
	   System.out.println("Sum of all prime numbers is " + sumOfPrimes + " and count of prime numbers in the given array is " + primeCount);
	    }
}




