import java.util.Scanner;
class Array8{
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
	    System.out.println("Enter the size of the array:");
	     int size = scanner.nextInt();
	     char[] charArray = new char[size];
             System.out.println("Enter the elements of the array:");
             for (int i = 0; i < size; i++) {
		     charArray[i] = scanner.next().charAt(0);
             }
	      System.out.println("Before Reverse:");
	              for (int i = 0; i < size; i += 2) {
		          System.out.print(charArray[i] + " ");
		      }
		          System.out.println();
		         for (int i = 0, j = size - 1; i < j; i++, j--) {
				 char temp = charArray[i];
				 charArray[i] = charArray[j];
				 charArray[j] = temp;
			 }
			 System.out.println("After Reverse:");
			   for (int i = 0; i < size; i += 2) {
		        	 System.out.print(charArray[i] + " ");
			   }
	    }
}





