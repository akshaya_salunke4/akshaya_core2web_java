import java.util.Scanner;
class Array9{
	    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the size of the array:");
	    int size = scanner.nextInt();
	    int[] arr = new int[size];
	    System.out.println("Enter the elements of the array:");
	    for (int i = 0; i < size; i++) {
		    arr[i] = scanner.nextInt();
	    }
	    int palindromeCount = 0;
             for (int i = 0; i < size; i++) {
         	    int num = arr[i];
		    int reverseNum = 0;
        	    int temp = num;
        	    while (temp != 0) {
		    reverseNum = reverseNum * 10 + temp % 10;
			    temp /= 10;
		    }
		    if (num == reverseNum) {
			    palindromeCount++;
		    }
	    }
	     System.out.println("Count of palindrome elements is: " + palindromeCount);

	 }
}



