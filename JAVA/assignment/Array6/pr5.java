import java.util.Scanner;
class Array5{
	    public static void main(String[] args) {
             Scanner sc = new Scanner(System.in);
	    System.out.println("Enter the size of the first array:");
	    int size1 = sc.nextInt();
	    System.out.println("Enter the elements of the first array:");
	    int[] arr1 = new int[size1];
	    for (int i = 0; i < size1; i++) {
			    arr1[i] = sc.nextInt();
		    }
	    System.out.println("Enter the size of the second array:");
		    int size2 = sc.nextInt();
		    System.out.println("Enter the elements of the second array:");
		    int[] arr2 = new int[size2];
		    for (int i = 0; i < size2; i++) {
                  	    arr2[i] = sc.nextInt();
		    }
                 int[] mergedArray = new int[size1 + size2];
              	    for (int i = 0; i < size1; i++) {
		    mergedArray[i] = arr1[i];
		    }
		    for (int i = 0; i < size2; i++) {
			    mergedArray[size1 + i] = arr2[i];
		    }
		    System.out.println("Array after merger:");
                   for (int i = 0; i < mergedArray.length; i++) {
         	    System.out.print(mergedArray[i]);
		    if (i < mergedArray.length - 1) {
				    System.out.println(" ");
			    }
		    }
	    }
}
