import java.util.Scanner;
class Array3{
	    public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
            System.out.println("Enter the size of the array:");
	    int size = sc.nextInt();
	    System.out.println("Enter the elements of the array:");
	    int[] arr = new int[size];
	    for (int i = 0; i < size; i++) {
		    arr[i] = sc.nextInt();
	    }
	    System.out.println("Enter the key:");
	    int key = sc.nextInt();
	    int count = 0;
	    for (int i = 0; i < size; i++) {
		    if (arr[i] == key) {
			    count++;
		    }
	    }
               if (count > 2) {
         	    for (int i = 0; i < size; i++) {
                        if (arr[i] == key) {
				    arr[i] = key * key * key;
	    		}
	    }
       }
	    }
}
